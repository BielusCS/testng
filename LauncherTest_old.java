package operaLauncher;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

public class LauncherTest {

	private Launcher l;
	
	
	@BeforeMethod
	public void beforeTest() {
		this.l = new Launcher();
	}

	@AfterMethod
	public void afterTest() {
		try {
			this.l.closeDriver();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	  }

	@Test(priority = 5)
	public void clickBtnTest() {
	    //throw new RuntimeException("Test not implemented");
		this.l.connectToOpera();
		this.l.navigateToGoogle();
		this.l.findElementBy("name", "q");
		this.l.sendKeys("Tutorials Point");
		try {
			this.l.clickBtn();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Test(priority = 10)
	public void closeDriverTest() {
	    //throw new RuntimeException("Test not implemented");
		this.l.connectToOpera();
		try {
			this.l.closeDriver();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Test(priority = -15)
	public void connectToOperaTest() {
		//throw new RuntimeException("Test not implemented");
		this.l.connectToOpera();
	}

	@Test(priority = -5)
	public void findElementByTest() {
		//throw new RuntimeException("Test not implemented");
		this.l.connectToOpera();
		this.l.navigateToGoogle();
		this.l.findElementBy("name", "q");
	}

	@Test(priority = -10)
	public void navigateToGoogleTest() {
		//throw new RuntimeException("Test not implemented");
		this.l.connectToOpera();
		this.l.navigateToGoogle();
	}

	@Test(priority = 0)
	public void sendKeysTest() {
		//throw new RuntimeException("Test not implemented");
		this.l.connectToOpera();
		this.l.navigateToGoogle();
		this.l.findElementBy("name", "q");
		this.l.sendKeys("Tutorials Point");
	}
}
