/**
 * 
 */
package operaLauncher;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.opera.OperaDriver;

/**
 * @author usuari
 *
 */
public class Launcher {
	
	private WebDriver wd;
    private WebElement we;
    
    public void connectToOpera() {
        System.setProperty("webdriver.opera.driver", "/home/usuari/NetBeansProjects/operadriver");
        this.wd = new OperaDriver();
    }
    
    public void navigateToGoogle() {
        this.wd.navigate().to("http://www.google.com");
    }
    
    /*
    public void findElementBy(String tipo, String id) {
        if (tipo.equalsIgnoreCase("name"))
            this.we = this.wd.findElement(By.name(id));
        else
            this.we = this.wd.findElement(By.className(id));
    }
    */
    
    public void findElementBy(String id) {
        this.we = this.wd.findElement(By.name(id));
    }
    
    public void sendKeys(String keys) {
    	this.we.sendKeys(keys);
    }
    
    public void clickBtn() throws InterruptedException {
        Thread.sleep(3000);
        this.we = this.wd.findElement(By.name("btnK"));
        this.we.click();
        //this.wd.findElement(By.name("btnK")).click();
    }
    
    public void closeDriver() throws InterruptedException {
        Thread.sleep(3000);
        this.wd.close();
    }
}
